﻿#include <iostream>
#include <cmath>

class MyClass
{
private:
    std::string name = "Evgeni";
    int age = 19;
public:
    MyClass(std::string _name, int _age): name(_name)
    {
        if (_age <= 0)
        {
            std::cout << "Invalid age!" << std::endl;
        }
        else age = _age;
    }
    
    int getAge()
    {
        return age;
    }
    std::string getName()
    {
        return name;
    }
    void printData()
    {
        std::cout << "Age: " << age << std::endl;
        std::cout << "Name: " << name << std::endl;
    }
  
};
class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << " " << y << " " << z << std::endl;
    }
    double length()
    {
        return sqrt(x * x + y * y + z * z);
    }
};

int main()
{
    MyClass i("Kirill", 21);
    i.printData();

    Vector vec(0, 3, 4);
    double vec_abs = vec.length();
    std::cout << vec_abs << std::endl;
   
}


